package data;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import bll.Order;



public class TableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String[] columnNames = { "ID", "Date","TableNr","Total" };
	private ArrayList<Order> orders;

	public  TableModel() {
		orders = new ArrayList<Order>();
	}

	public  TableModel(ArrayList<Order> orders) {
		this.orders = orders;
	}

	@Override
	public  int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public  int getRowCount() {
		return orders.size();
	}

	public  String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public  Object getValueAt(int row, int column) {
		Order order = getOrder(row);
		switch (column) {
		case 0:
			return order.getId();
		case 1:
			return order.getDate();
		case 2:
			return order.getTable();
		case 3: 
			return order.getTotal();
			
		}
		return order;
	}

	public  Order getOrder(int row) {
		return orders.get(row);
	}

	public  void addOrder(Order order) {
		insertOrder(getRowCount(), order);
	}

	public  void insertOrder(int row, Order order) {
		orders.add(row, order);
		fireTableRowsInserted(row, row);
	}

	public  void removeOrder(int row) {
		orders.remove(row);
		fireTableRowsDeleted(row, row);
	}

}
