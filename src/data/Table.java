package data;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Table extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public  Table(TableModel Model) {
		JTable Table = new JTable(Model);
		JScrollPane scr = new JScrollPane(Table);
		add(scr);
		setVisible(true);
	}

}