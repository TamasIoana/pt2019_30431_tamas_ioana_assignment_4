package bll;

public interface IRestaurantProcessing {
	
	/**
	 * @pre order != null
	 * @post orders.size() = orders.size()@pre + 1
	 * @post get(order) != null
	 */
	void addOrder(Order order);
	
	/**
	 * @pre menuItem != null
	 * @pre containsKey(menuItem)
	 * @post items.size() = items.size()@pre - 1
	 * @post !containsKey(menuItem)
	 */
	
	public void removeMenuItem(MenuItem menuItem);

	/**
	 * @pre o != null
	 * @pre containsKey(o)
	 * @post orders.size() = orders.size()@pre - 1
	 * @post !containsKey(o)
	 */
	
	public void removeOrder(Order o);
	
	
	void generateReport();

	
	

}
