package bll;

import java.io.Serializable;
import java.util.Observable;

public class Order extends Observable implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	private String date;
	private int id;
	private int table;
	private double total;
	
	public Order(int id,String date, int table, double total ) {
		this.date=date;
		this.id=id;
		this.table=table;
		this.total=total;
		
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	public String toString() {
		return String.format("ID:%d  DATE:%s TABLE:%d TOTAL:%d", id,date,table,total);
	}
	

}
