package bll;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
public class Restaurant implements IRestaurantProcessing, Serializable, Observer{
	
	private static final long serialVersionUID = 1L;
	
	private ArrayList<MenuItem>items;
	private ArrayList<Order>orders;
	private Hashtable<Order, ArrayList<MenuItem>>hashtable;
	private String observerReport;
	
	
	public Restaurant() {
		items=new ArrayList<MenuItem>();
		orders= new ArrayList<Order>();
		hashtable=new Hashtable<Order, ArrayList<MenuItem>>();
		}
	
	public ArrayList<Order> getOrders(){
		return orders;
		
	}
	public ArrayList<MenuItem> getMenuItems(){
		return items;
	}
	
	public Hashtable<Order, ArrayList<MenuItem>> getHashtable(){
		return hashtable;
	}


	@Override
	public void addOrder(Order order) {
		// TODO Auto-generated method stub
		assert isWellFormed();
		
		assert order != null;
		int size = orders.size();
		orders.add(order);
		hashtable.put(order, new ArrayList<MenuItem>());
		serialize();
		assert orders.size()==orders.size()+1;
		assert isWellFormed();
		
		
	}
	
	@Override
	public void removeOrder(Order o) {
assert isWellFormed();
		
		assert o != null;
		assert hashtable.containsKey(o);
		int size = hashtable.size();

		orders.remove(o);
		hashtable.remove(o, hashtable.get(o));

		assert !hashtable.containsKey(o);
		assert hashtable.size() == size - 1;
		assert isWellFormed();
		
	}
	
	



	@Override
	public void removeMenuItem(MenuItem menuItem) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean isWellFormed() {
		// TODO Auto-generated method stub
		for(Entry<Order, ArrayList<MenuItem>> entry:hashtable.entrySet()) {
			if(entry.getValue().isEmpty()) {
				return false;
			}
		}
		return false;
	}
	
	public  void serialize() {
		try {
			FileOutputStream fileOut = new FileOutputStream("restaurant.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	
	public  Restaurant deserialize() {
		Restaurant  rest = null;
		try {
			FileInputStream fileIn = new FileInputStream("restaurant.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			rest = (Restaurant) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		}
		return rest;
	}

	
	
	@Override
	public void generateReport() {
		String data = null;
		for (Order order : orders) {
			data = order.getId() + " " + order.getTotal() + "\n";
			System.out.println(data);
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		observerReport = "\n" + "A modification has occured at order with the id: " + ((Order) o).getId()+ "\n";
		serialize();
	}
	
	public  String getObserverReport() {
		return observerReport;
	}


	


}
