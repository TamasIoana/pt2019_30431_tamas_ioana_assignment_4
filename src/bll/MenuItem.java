package bll;

import java.io.Serializable;

public  abstract class MenuItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private int id;
	private double price;
	
	public MenuItem(String name, int id, double price) {
		this.name=name;
		this.id=id;
		this.price=price;
		
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String toString() {
		return String.format("ID:%d  Name:%s Price:%.2f", id, name,price);
	}
	
	public abstract void computePrice();


	
	
	

}
