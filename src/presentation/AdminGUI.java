package presentation;

import bll.MenuItem;
import bll.Restaurant;
import data.Table;
import data.TableModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;



public class AdminGUI implements ActionListener{

	private JFrame frame = new JFrame("MenuItem Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions1 = new JPanel();
	private JPanel actions2 = new JPanel();
	private JPanel table = new JPanel();
	private JPanel infoTablePanel = new JPanel();
	
	private JLabel nameLabel = new JLabel("Introduce name:");
	private JLabel idLabel = new JLabel("Introduce id:");
	private JLabel priceLabel=new JLabel("Introduce price");
	

	private JTextField nameText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField priceText = new JTextField();

	
	private int menuItemID;
	private String name;
	private double price;
	
	private Restaurant rest;
	
	TableModel tableModel = new TableModel();
	Table itemstable;
	
	private JButton addItem = new JButton("Add Item ");
	private JButton removeItem = new JButton("Remove Item");
	private JButton editItem = new JButton("Edit Item");
	private JButton viewItems = new JButton("View Items");
	
	public AdminGUI(Restaurant rest) {
		this.rest = rest;
		
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(5, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(nameLabel);
		info.add(nameText);
		info.add(idLabel);
		info.add(idText);
		info.add(priceLabel);
		info.add(priceText);
		
		
		nameText.addActionListener(this);
		idText.addActionListener(this);
		priceText.addActionListener(this);
		
		
		actions1.setLayout(new FlowLayout());
		
		actions1.add(addItem);
		actions1.add(removeItem);
		actions1.add(editItem);
		actions1.add(viewItems);
		
		addItem.setActionCommand("addItem");
		removeItem.setActionCommand("removeItem");
		editItem.setActionCommand("editItem");
		viewItems.setActionCommand("viewItems");
		
		addItem.setBackground(Color.WHITE);
		removeItem.setBackground(Color.WHITE);
		editItem.setBackground(Color.WHITE);
		viewItems.setBackground(Color.WHITE);
		
		addItem.addActionListener(this);
		removeItem.addActionListener(this);
		editItem.addActionListener(this);
		viewItems.addActionListener(this);
		

		
		table.setVisible(false);
		infoTablePanel.setVisible(false);
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions1, BorderLayout.WEST);
	
		frame.add(table, BorderLayout.SOUTH);
		frame.add(infoTablePanel, BorderLayout.SOUTH);
	}
	
	public  void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("addItem")) {
			
			Random rand=new Random();
			name=nameText.getText();
			price=Double.parseDouble(priceText.getText());
			//MenuItem item=new MenuItem(name,rand.nextInt(100),price);
		//	rest.serialize();
			/*
			//AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
			type = typeText.getText();
			PIN = pinText.getText();
			//sum = Double.parseDouble(sumText.getText());
			
			MenuItem foundPerson = null;
			Random randomGenerator = new Random();
			boolean found = false;
			
			for (MenuItem p : rest.getClients()) {
				if (p.getName().equals(name)) {
					found = true;
					foundPerson = p;
				}
			}

			if (!found) {
				Person newClient = new Person(randomGenerator.nextInt(100), name);
				bank.addPerson(newClient);
				bank.addHolderAccount(newClient, type, PIN);
				updateInfo(newClient);
			} else {
				bank.addHolderAccount(foundPerson, type, PIN);
				updateInfo(foundPerson);
			}
			bank.serialize();
			*/
			
		}
		else if (e.getActionCommand().equals("removeItem")) {
			/*
			AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
			
			
			Person person = null;
			Account account = null;

			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
						}
					}
				}
			}
			bank.removeHolderAccount(person, account);
			updateInfo(person);
			bank.serialize();
			*/
		}
		
		/*else if (e.getActionCommand().equals("viewAcc")) {
			//AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
		}*/
		else if (e.getActionCommand().equals("editItem")) {
			/*
			//name = nameText.getText();
			PIN = pinText.getText();
			sum = Double.parseDouble(sumText.getText());
			
			Person person = null;
			Account account = null;
	
			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
							account.deposit(person, account, sum);
							bank.update(account, bank);
						}
					}
				}
			}
			System.out.println(bank.getObserverReport());
			bank.serialize();
			*/
		}
		else if (e.getActionCommand().equals("viewItems")) {
			/*
			//name = nameText.getText();
			PIN = pinText.getText();
			sum = Double.parseDouble(sumText.getText());
			
			Person person = null;
			Account account = null;

			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
							if (!account.cannotWithdraw(sum)) {
								JOptionPane.showMessageDialog(null, "The total of the account is too small for this operation");
							} else {
								account.withdraw(person, account, sum);
							}
							bank.update(account, bank);
						}
					}
				}
			}
			System.out.println(bank.getObserverReport());
			bank.serialize();
			*/
		}
	}
	
	
	public  void updateInfo(MenuItem menuItem) {
	/*
		
			for (int j = tableModel.getRowCount() - 1; j >= 0; j--) {
				tableModel.removeItem(j);
			}
			ArrayList<MenuItem> items;
			items = rest.getItems();
			for (MenuItem p : items) {
				tableModel.addItem(p);
			}
			itemstable = new Table(tableModel);
			table.add(itemstable);
			table.setVisible(true);
			infoTablePanel.setVisible(false);
			frame.add(table, BorderLayout.SOUTH);
			*/
		}
		
}