package presentation;

import bll.Restaurant;

import bll.MenuItem;
import bll.Order;
import bll.Restaurant;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import data.Table;
import data.TableModel;

public class WaiterGUI implements ActionListener{

	private JFrame frame = new JFrame("Order Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions1 = new JPanel();

	private JPanel table = new JPanel();
	private JPanel infoTablePanel = new JPanel();
	
	private JLabel dateLabel = new JLabel("Introduce date:");
	private JLabel idLabel = new JLabel("Introduce id:");
	private JLabel priceLabel=new JLabel("Introduce price");
	private JLabel tableLabel=new JLabel("Introduce table number");
	

	private JTextField dateText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField priceText = new JTextField();
	private JTextField tableText = new JTextField();

	
	private int orderID;
	private String date;
	private double orderPrice;
	private int tableNumber;
	
	private Restaurant rest;
	
	//InfoTableModel infoTableModel = new InfoTableModel();
	//InfoTable infoTable;
	
	private JButton addOrder = new JButton("Add Order ");
	private JButton editOrder= new JButton("Remove Order");
	private JButton viewOrders = new JButton("View Orders");
	
	TableModel tableModel = new TableModel();
	Table orderstable;
	
	public WaiterGUI(Restaurant rest) {
		this.rest = rest;
		
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(5, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(dateLabel);
		info.add(dateText);
		info.add(idLabel);
		info.add(idText);
		info.add(priceLabel);
		info.add(priceText);
		info.add(tableLabel);
		info.add(tableText);
		
		
		dateText.addActionListener(this);
		idText.addActionListener(this);
		priceText.addActionListener(this);
		tableText.addActionListener(this);
		
		
		actions1.setLayout(new FlowLayout());
		
		actions1.add(addOrder);
		actions1.add(editOrder);
		actions1.add(viewOrders);
		
		addOrder.setActionCommand("addOrder");
		editOrder.setActionCommand("removeOrder");
		viewOrders.setActionCommand("viewOrders");
		
		addOrder.setBackground(Color.WHITE);
		editOrder.setBackground(Color.WHITE);
		viewOrders.setBackground(Color.WHITE);
		
		addOrder.addActionListener(this);
		editOrder.addActionListener(this);
		viewOrders.addActionListener(this);
		

		
		table.setVisible(false);
		infoTablePanel.setVisible(false);
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions1, BorderLayout.WEST);
	
		frame.add(table, BorderLayout.SOUTH);
		frame.add(infoTablePanel, BorderLayout.SOUTH);
	}
	
	public  void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("addOrder")) {
			
			orderID = Integer.parseInt(idText.getText());
			date = dateText.getText();
			orderPrice = Double.parseDouble(priceText.getText());
			tableNumber = Integer.parseInt(tableText.getText());
			
			Random randomGenerator = new Random();
			Order newOrder = new Order(randomGenerator.nextInt(100),date, tableNumber,orderPrice);
			rest.addOrder(newOrder);
			updateOrders();
			rest.serialize();
			
			
		}
	
		else if (e.getActionCommand().equals("removeOrder")) {
			
			orderID = Integer.parseInt(idText.getText());
			date = dateText.getText();
			orderPrice = Double.parseDouble(priceText.getText());
			tableNumber = Integer.parseInt(tableText.getText());
			
			boolean found = false;
			Order foundOrder = null;

			for (Order order : rest.getOrders()) {
				if (order.getId() == orderID) {
					found = true;
					foundOrder = order;
				}
			}
			if (found) {
				rest.removeOrder(foundOrder);
				updateOrders();
			}

			rest.serialize();
		}
			
		

		else if (e.getActionCommand().equals("viewOrders")) {
			
	
			
		}
	}
	
	
	private void updateOrders() {
		for (int j = tableModel.getRowCount() - 1; j >= 0; j--) {
			tableModel.removeOrder(j);
		}
		ArrayList<Order> orders;
		orders = rest.getOrders();
		for (Order o : orders) {
			tableModel.addOrder(o);
		}
		orderstable = new Table(tableModel);
		table.add(orderstable);
		table.setVisible(true);
		infoTablePanel.setVisible(false);
		//table.setSize(850, 400);
		//setVisible(true);
		frame.add(table, BorderLayout.SOUTH);
	}
		
	


}