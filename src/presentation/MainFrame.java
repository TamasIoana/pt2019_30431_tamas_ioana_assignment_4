package presentation;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import bll.Restaurant;



public class MainFrame implements ActionListener{

	private JFrame frame = new JFrame();
	private JPanel panel = new JPanel();
	private JPanel mainPanel = new JPanel();
	
	private JButton AdminProc;
	private JButton WaiterProc;
	private JButton ChefProc;
	
	private Restaurant restaurant;
	
	public MainFrame(Restaurant restaurant) {
		this.restaurant = restaurant;
		
		frame.setTitle("Restaurant Management");//the title of the frame
		frame.setVisible(true);//very important! we want to see all the components of our frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//this is meant for when the close button is pushed, the frame to close
		frame.setSize(800, 300);//the size of the frame
		frame.setLocationRelativeTo(null);//it places the cursor at the center of the frame
		
		AdminProc = new JButton("Admin Ops");
		WaiterProc = new JButton("Waiter Ops");
		ChefProc = new JButton("Chef Ops");
		
		AdminProc.setActionCommand("process admin");
		WaiterProc.setActionCommand("process waiter");
		ChefProc.setActionCommand("process chef");
		
		AdminProc.addActionListener(this);
		ChefProc.addActionListener(this);
		WaiterProc.addActionListener(this);
		
		AdminProc.setBackground(Color.WHITE);
		AdminProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		WaiterProc.setBackground(Color.WHITE);
		WaiterProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		ChefProc.setBackground(Color.WHITE);
		ChefProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		panel.setLayout(new GridLayout(1,2));
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel.setVisible(true);
		
		panel.add(AdminProc);
		panel.add(WaiterProc);
		panel.add(ChefProc);
		
		frame.add(panel);
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("process admin")) {
			mainPanel.setVisible(false);
			AdminGUI admingui = new AdminGUI(restaurant);
		}
		else if (e.getActionCommand().equals("process waiter")) {
			WaiterGUI waitergui = new WaiterGUI(restaurant);
		}
		else if (e.getActionCommand().equals("process chef")) {
		ChefGUI chefgui = new ChefGUI(restaurant);
		}
		
	}
	
}
